const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const { success, fail, error } = require("../config/response");

const model = init_models(sequelize);

const getUserLikeRes = async (req, res) => {
  try {
    let { id } = req.params;
    let data = await model.user.findOne({
      where: {
        user_id: id,
      },
      attributes: ["user_id", "full_name"],
      include: ["res_id_restaurants"],
    });

    success(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

const getResUserLike = async (req, res) => {
  try {
    let { id } = req.params;
    let data = await model.restaurant.findOne({
      where: {
        res_id: id,
      },
      attributes: ["res_id", "res_name"],
      include: ["user_id_users"],
    });

    success(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

const postLikeRes = async (req, res) => {
  try {
    let { user_id, res_id, date_like } = req.body;
    let checkUser = await model.like_res.findOne({
      where: {
        user_id,
        res_id,
      },
    });

    if (!checkUser) {
      let data = await model.like_res.create({
        user_id,
        res_id,
        date_like,
      });

      success(res, data, "Like thành công!");
    } else {
      fail(res, "", "Người dùng đã like nhà hàng này!");
    }
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

const deleteLikeRes = async (req, res) => {
  try {
    let { user_id, res_id } = req.body;
    let checkUser = await model.like_res.findOne({
      where: {
        user_id,
        res_id,
      },
    });

    if (checkUser) {
      let data = await model.like_res.destroy({
        where: {
          user_id,
          res_id,
        },
      });

      success(res, data, "Đã huỷ like!");
    } else {
      fail(res, "", "Người dùng chưa like nhà hàng này!");
    }
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

module.exports = { getUserLikeRes, getResUserLike, postLikeRes, deleteLikeRes };
