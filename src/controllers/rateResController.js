const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const { success, fail, error } = require("../config/response");

const model = init_models(sequelize);

const getUserRateRes = async (req, res) => {
  try {
    let { id } = req.params;
    let data = await model.user.findOne({
      where: {
        user_id: id,
      },
      attributes: ["user_id", "full_name"],
      include: ["res_id_restaurant_rate_res"],
    });

    success(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

const getResUserRate = async (req, res) => {
  try {
    let { id } = req.params;
    let data = await model.restaurant.findOne({
      where: {
        res_id: id,
      },
      attributes: ["res_id", "res_name"],
      include: ["user_id_user_rate_res"],
    });

    success(res, data, "Lấy dữ liệu thành công!");
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

const postRateRes = async (req, res) => {
  try {
    let { user_id, res_id, amount, date_rate } = req.body;
    let checkRate = await model.rate_res.findOne({
      where: {
        user_id,
        res_id,
      },
    });

    if (!checkRate) {
      let data = await model.rate_res.create({
        user_id,
        res_id,
        amount,
        date_rate,
      });

      success(res, data, "Thêm đánh giá thành công!");
    } else {
      fail(res, "", "Người dùng đã đánh giá nhà hàng này!");
    }
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

module.exports = { getUserRateRes, getResUserRate, postRateRes };
