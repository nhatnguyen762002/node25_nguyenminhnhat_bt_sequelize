const sequelize = require("../models/index");
const init_models = require("../models/init-models");
const { success, fail, error } = require("../config/response");

const model = init_models(sequelize);

const postOrder = async (req, res) => {
  try {
    let { user_id, food_id, amount, code, arr_sub_id } = req.body;
    let checkOrder = await model.order.findOne({
      where: {
        user_id,
        food_id,
      },
    });

    if (!checkOrder) {
      let data = await model.order.create({
        user_id,
        food_id,
        amount,
        code,
        arr_sub_id,
      });

      success(res, data, "Đặt món thành công!");
    } else {
      fail(res, "", "User đã đặt món này!");
    }
  } catch (err) {
    error(res, "Lỗi BE!");
  }
};

module.exports = { postOrder };
