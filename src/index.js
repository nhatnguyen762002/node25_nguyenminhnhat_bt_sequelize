const express = require("express");
const cors = require("cors");
const rootRoute = require("./routes");

const app = express();

app.use(express.json());
app.use(express.static("."));
app.use(cors());
app.use("/api", rootRoute);
app.listen(8080);
