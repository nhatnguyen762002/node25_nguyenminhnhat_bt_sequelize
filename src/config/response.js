const success = (res, data, message) => {
  res.status(200).json({ message, content: data });
};

const fail = (res, data, message) => {
  res.status(400).json({
    message,
    content: data,
  });
};

const error = (res, message) => {
  res.status(500).send(message);
};

module.exports = {
  success,
  fail,
  error,
};
