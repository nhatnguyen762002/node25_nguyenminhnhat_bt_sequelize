const express = require("express");
const {
  getUserRateRes,
  getResUserRate,
  postRateRes,
} = require("../controllers/rateResController");

const rateResRoute = express.Router();

rateResRoute.get("/getUserRateRes/:id", getUserRateRes);
rateResRoute.get("/getResUserRate/:id", getResUserRate);

rateResRoute.post("/postRateRes", postRateRes);

module.exports = rateResRoute;
