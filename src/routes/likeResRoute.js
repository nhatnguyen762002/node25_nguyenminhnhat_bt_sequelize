const express = require("express");
const {
  getUserLikeRes,
  getResUserLike,
  postLikeRes,
  deleteLikeRes,
} = require("../controllers/likeResController");

const likeResRoute = express.Router();

likeResRoute.get("/getUserLikeRes/:id", getUserLikeRes);
likeResRoute.get("/getResUserLike/:id", getResUserLike);

likeResRoute.post("/postLikeRes", postLikeRes);

likeResRoute.delete("/deleteLikeRes", deleteLikeRes);

module.exports = likeResRoute;
