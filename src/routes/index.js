const express = require("express");
const likeResRoute = require("./likeResRoute");
const orderRoute = require("./orderRoute");
const rateResRoute = require("./rateResRoute");

const rootRoute = express.Router();

rootRoute.use("/likeRes", likeResRoute);
rootRoute.use("/rateRes", rateResRoute);
rootRoute.use("/order", orderRoute);

module.exports = rootRoute;
